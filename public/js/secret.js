
window.onload = function(){
    document.body.style.opacity = 1;
    document.getElementById('gameBtn').addEventListener('click',function(){
        document.body.style.opacity = 0;
        setTimeout(function(){
            document.location.href = "game.html";
        },1500);
    });
    firebase.auth().onAuthStateChanged(function (user) {
        var userId = firebase.auth().currentUser.uid;
        var currentRef = firebase.database().ref('currentChat/' + userId);
        var chatId;
        currentRef.once('value').then(function(snapshot){
            chatId = snapshot.val().chatId;
            // process chat content
            var post_btn = document.getElementById('post_btn');
            var post_txt = document.getElementById('comment');
            var contentRef = firebase.database().ref('chatHistory');
            var friendRef = firebase.database().ref('friend/' + userId + '/' + chatId);
            var friendRef2 = firebase.database().ref('friend/' + chatId + '/' + userId);
            // display message
            // other say
            var str_before_message_id = '<div class="content othersay" data-placement="right" data-html="true" data-trigger="focus" id=';
            var str_before_message = '>';
            var before_thumb_up_id = '</div><img src="img/like.png" width="20px" height="20px" class="thumb" id=';
            var before_thumb_down_id = '><img src="img/unlike.png" width="20px" height="20px" class="thumb" id=';
            var str_after_content = '><div></div>';
            // my say
            var mysay_before_message = '<div class="content mysay"><span>';
            var mysay_before_thumb_up_id = '</span></div><img src="img/like.png" width="20px" height="20px" class="thumb mysay" id=';
            var mysay_before_thumb_down_id = '><img src="img/unlike.png" width="20px" height="20px" class="thumb mysay" id=';
            var mysay_after_content = '><div class="clear"></div>';
            // List for store posts html
            var total_post = [];
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;
            var chat_content = document.getElementById('chat_content');
            var total_btn_id = [];

            contentRef.once('value').then(function(snapshot) {
                    snapshot.forEach(e => {
                        if((userId == e.val().member1 && chatId == e.val().member2) || (userId == e.val().member2 && chatId == e.val().member1)){
                            var mesRef = firebase.database().ref('chatHistory/' + e.key + '/' + 'message');
                            mesRef.once('value').then(function(s){
                                s.forEach(element => {
                                    var usernameRef = firebase.database().ref('user_info/' + element.val().speaker);
                                    usernameRef.once('value').then(function(ss){
                                        var message_id = element.key;
                                        var thumb_up_id = element.key + 'up';
                                        var thumb_down_id = element.key + 'down';
                                        var data = element.val().data.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
                                        if(userId == element.val().speaker){
                                            total_post.push(mysay_before_message + data + mysay_before_thumb_up_id + thumb_up_id + mysay_before_thumb_down_id + thumb_down_id +mysay_after_content);
                                        }
                                        else{
                                            total_post.push(str_before_message_id + message_id + str_before_message + data + before_thumb_up_id + thumb_up_id + before_thumb_down_id + thumb_down_id + str_after_content);
                                            total_btn_id.push(message_id);
                                        }
                                        first_count++;
                                        var newStr = total_post.join('');
                                        chat_content.innerHTML = newStr;
                                    }).then(function(){
                                        $('.othersay').tooltip({title: function(){return "<i class='far fa-thumbs-up'></i><i class='far fa-thumbs-down'></i>";} });
                                        total_btn_id.forEach(function(e){
                                            var btn = document.getElementById(e);
                                            btn.onclick = function(event){
                                                var id = event.target.id;
                                                $('#' + id).tooltip('toggle');
                                                setTimeout(function(){
                                                    var fa_up = document.querySelector('.fa-thumbs-up');
                                                    var fa_down = document.querySelector('.fa-thumbs-down');
                                                    fa_up.addEventListener('mouseover',function(){
                                                        this.style.transform = "scale(1.5,1.5)";
                                                    });
                                                    fa_up.addEventListener('mouseout',function(){
                                                        this.style.transform = "scale(1.0,1.0)";
                                                    });
                                                    fa_up.addEventListener('click',function(){
                                                        var thumbRef = firebase.database().ref('thumbState/'+id);
                                                        if(document.getElementById(id+'up').style.display == 'inline'){
                                                            thumbRef.set({
                                                                up: 0,
                                                                down: 0
                                                            });
                                                        }
                                                        else{
                                                            thumbRef.set({
                                                                up: 1,
                                                                down: 0
                                                            });
                                                        }
                                                        $('#' + id).tooltip('toggle');
                                                    });
                                                    fa_down.addEventListener('mouseover',function(){
                                                        this.style.transform = "scale(1.5,1.5)";
                                                    });
                                                    fa_down.addEventListener('mouseout',function(){
                                                        this.style.transform = "scale(1.0,1.0)";
                                                    });
                                                    fa_down.addEventListener('click',function(){
                                                        var thumbRef = firebase.database().ref('thumbState/'+id);
                                                        if(document.getElementById(id+'down').style.display == 'inline'){
                                                            thumbRef.set({
                                                                up: 0,
                                                                down: 0
                                                            });
                                                        }
                                                        else{
                                                            thumbRef.set({
                                                                up: 0,
                                                                down: 1
                                                            });
                                                        }
                                                        $('#' + id).tooltip('toggle');
                                                    });
                                                },100);
                                            };
                                        });
                                    });
                                });
                                mesRef.on('child_added',function(data){
                                    second_count++;
                                    if(second_count>first_count){
                                        var child_data = data.val();
                                        var _speaker = child_data.speaker;
                                        var userRef = firebase.database().ref('user_info/' + _speaker);
                                        userRef.once('value').then(function(ss){
                                            var message_id = data.key;
                                            var thumb_up_id = data.key + 'up';
                                            var thumb_down_id = data.key + 'down';
                                            var _data = child_data.data.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
                                            if(_speaker == userId){
                                                total_post[total_post.length]= mysay_before_message + _data + mysay_before_thumb_up_id + thumb_up_id + mysay_before_thumb_down_id + thumb_down_id +mysay_after_content;
                                            }
                                            else{
                                                total_post[total_post.length]= str_before_message_id + message_id + str_before_message + _data + before_thumb_up_id + thumb_up_id + before_thumb_down_id + thumb_down_id + str_after_content;
                                                total_btn_id[total_btn_id.length] = data.key;
                                            }
                                            newStr = total_post.join('');
                                            chat_content.innerHTML = newStr;
                                        }).then(function(){
                                            $('.othersay').tooltip({title: function(){return "<i class='far fa-thumbs-up'></i><i class='far fa-thumbs-down'></i>";} });
                                            // html被更新，重新判斷一次
                                            var thumbRef = firebase.database().ref('thumbState');
                                            thumbRef.once('value').then(function(snapshot){
                                                snapshot.forEach(function(e){
                                                    var thumb_up = document.getElementById(e.key + 'up');
                                                    var thumb_down = document.getElementById(e.key + 'down');
                                                    if(thumb_up && thumb_down){
                                                        if(e.val().up == 0 && e.val().down == 0){
                                                            thumb_up.style.display = '';
                                                            thumb_down.style.display = '';
                                                        }
                                                        else if(e.val().up == 1){
                                                            thumb_up.style.display = 'inline';
                                                            thumb_down.style.display = '';
                                                        }
                                                        else{
                                                            thumb_up.style.display = '';
                                                            thumb_down.style.display = 'inline';
                                                        }
                                                    }
                                                });
                                            });
                                            total_btn_id.forEach(function(e){
                                                var btn = document.getElementById(e);
                                                btn.onclick = function(event){
                                                    var id = event.target.id;
                                                    $('#' + id).tooltip('toggle');
                                                    setTimeout(function(){
                                                        var fa_up = document.querySelector('.fa-thumbs-up');
                                                        var fa_down = document.querySelector('.fa-thumbs-down');
                                                        fa_up.addEventListener('mouseover',function(){
                                                            this.style.transform = "scale(1.5,1.5)";
                                                        });
                                                        fa_up.addEventListener('mouseout',function(){
                                                            this.style.transform = "scale(1.0,1.0)";
                                                        });
                                                        fa_up.addEventListener('click',function(){
                                                            var thumbRef = firebase.database().ref('thumbState/'+id);
                                                            if(document.getElementById(id+'up').style.display == 'inline'){
                                                                thumbRef.set({
                                                                    up: 0,
                                                                    down: 0
                                                                });
                                                            }
                                                            else{
                                                                thumbRef.set({
                                                                    up: 1,
                                                                    down: 0
                                                                });
                                                            }
                                                            $('#' + id).tooltip('toggle');
                                                        });
                                                        fa_down.addEventListener('mouseover',function(){
                                                            this.style.transform = "scale(1.5,1.5)";
                                                        });
                                                        fa_down.addEventListener('mouseout',function(){
                                                            this.style.transform = "scale(1.0,1.0)";
                                                        });
                                                        fa_down.addEventListener('click',function(){
                                                            var thumbRef = firebase.database().ref('thumbState/'+id);
                                                            if(document.getElementById(id+'down').style.display == 'inline'){
                                                                thumbRef.set({
                                                                    up: 0,
                                                                    down: 0
                                                                });
                                                            }
                                                            else{
                                                                thumbRef.set({
                                                                    up: 0,
                                                                    down: 1
                                                                });
                                                            }
                                                            $('#' + id).tooltip('toggle');
                                                        });
                                                    },100);
                                                };
                                            });
                                        });
                                    }
                                });
                                var thumbRef = firebase.database().ref('thumbState');
                                thumbRef.once('value').then(function(snapshot){
                                    snapshot.forEach(function(e){
                                        var thumb_up = document.getElementById(e.key + 'up');
                                        var thumb_down = document.getElementById(e.key + 'down');
                                        if(thumb_up && thumb_down){
                                            if(e.val().up == 0 && e.val().down == 0 ){
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = '';
                                            }
                                            else if(e.val().up == 1){
                                                thumb_up.style.display = 'inline';
                                                thumb_down.style.display = '';
                                            }
                                            else{
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = 'inline';
                                            }
                                        }
                                    });
                                    thumbRef.on('child_changed',function(element){
                                        var thumb_up = document.getElementById(element.key + 'up');
                                        var thumb_down = document.getElementById(element.key + 'down');
                                        if(thumb_up && thumb_down){
                                            if(element.val().up == 0 && element.val().down == 0){
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = '';
                                            }
                                            else if(element.val().up == 1){
                                                thumb_up.style.display = 'inline';
                                                thumb_down.style.display = '';
                                            }
                                            else{
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = 'inline';
                                            }
                                        }
                                    });
                                    thumbRef.on('child_added',function(element){
                                        var thumb_up = document.getElementById(element.key + 'up');
                                        var thumb_down = document.getElementById(element.key + 'down');
                                        if(thumb_up && thumb_down){
                                            if(element.val().up == 0 && element.val().down == 0){
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = '';
                                            }
                                            else if(element.val().up == 1){
                                                thumb_up.style.display = 'inline';
                                                thumb_down.style.display = '';
                                            }
                                            else{
                                                thumb_up.style.display = '';
                                                thumb_down.style.display = 'inline';
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
                })
                .catch(function(e){
                    console.log(e.message);
                });
            // click button
            post_btn.addEventListener('click', function () {
                if (post_txt.value != "") {
                    var tmp = post_txt.value;
                    friendRef.once('value').then(function(s){
                        if(s.val()==null){
                            var newRef = contentRef.push();
                            newRef.set({
                                member1: userId,
                                member2: chatId
                            });
                            friendRef.set({
                                id: chatId 
                            });
                            friendRef2.set({
                                id: userId
                            });
                        }
                        contentRef.once('value').then(function(snapshot){
                            snapshot.forEach(e => {
                                if((userId == e.val().member1 && chatId == e.val().member2) || (userId == e.val().member2 && chatId == e.val().member1)){
                                    var mesRef = firebase.database().ref('chatHistory/' + e.key +'/' + 'message').push();
                                    mesRef.set({
                                        speaker: userId,
                                        data: post_txt.value
                                    });
                                    post_txt.value = "";
                                }
                            });
                        });
                        var notifyRef = firebase.database().ref('notify').push();
                        notifyRef.set({
                            sender: userId,
                            receiver: chatId,
                            message: tmp
                        });
                    });
                    
                }
            });
            // google notifictation
            var NotifyRef = firebase.database().ref('notify');
            NotifyRef.once('value').then(function(snapshot){
                var userId = firebase.auth().currentUser.uid;
                snapshot.forEach(e => {
                    if(e.val().receiver == userId){
                        if(e.val().sender==chatId){
                            firebase.database().ref('notify/'+ e.key).remove();
                        }
                        else{
                            var name2Ref = firebase.database().ref('user_info/' + e.val().sender);
                            name2Ref.once('value').then(function(ss){
                                var sender = ss.val().name;
                                var googleNotify = function(){
                                    var notification = new Notification(sender+'傳送了訊息', {
                                        body: e.val().message,
                                        icon: 'img/notify.png'
                                    });
                                    notification.onclick = function() {
                                        firebase.database().ref('notify/'+ e.key).remove();
                                        notification.close();    
                                    };
                                };
                                if (window.Notification) {
                                    if (Notification.permission == "granted") {
                                        googleNotify();
                                    }
                                    else if (Notification.permission != "denied") {
                                        Notification.requestPermission(function (permission) {
                                            googleNotify();
                                        });
                                    }
                                } else {
                                    create_alert('error','此瀏覽器不支援Notification');    
                                }
                            });
                        }
                    }
                });
                NotifyRef.on('child_added',function(shot){
                    if(shot.val().receiver == userId){
                        if(shot.val().sender == chatId){
                            firebase.database().ref('notify/'+ shot.key).remove();
                        }
                        else{
                            var name2Ref = firebase.database().ref('user_info/' + shot.val().sender);
                            name2Ref.once('value').then(function(ss){
                                var sender = ss.val().name;
                                var googleNotify = function(){
                                    var notification = new Notification(sender+'傳送了訊息', {
                                        body: shot.val().message,
                                        icon: 'img/notify.png'
                                    });
                                    notification.onclick = function() {
                                        firebase.database().ref('notify/'+ shot.key).remove();
                                        notification.close();    
                                    };
                                };
                                if (window.Notification) {
                                    if (Notification.permission == "granted") {
                                        googleNotify();
                                    }
                                    else if (Notification.permission != "denied") {
                                        Notification.requestPermission(function (permission) {
                                            googleNotify();
                                        });
                                    }
                                } else {
                                    create_alert('error','此瀏覽器不支援Notification');    
                                }
                            });
                        }
                    }
                });
            });
            });
            var back_btn = document.getElementById('back_btn');
            back_btn.addEventListener('click',function(){
                currentRef.remove();
            });
    });

}
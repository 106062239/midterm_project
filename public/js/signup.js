function initApp() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignUp = document.getElementById('btnSignUp');
    var inputName = document.getElementById('inputName');
    btnSignUp.addEventListener('click', function () {   
        let email = txtEmail.value.toString();
        let password = txtPassword.value.toString();
        let username = inputName.value.toString();
        let nameRef = firebase.database().ref('exist_name/' + username);
        if(email=="" || password=="" || username==""){
            create_alert('error','All are required');
        }
        else {
        nameRef.once('value').then(function(snapshot) {
            var data = snapshot.val();
            if(data != null){
                create_alert('error','This User Name is existed !');
            }
            else {
                firebase.auth().createUserWithEmailAndPassword(email,password).then(function(res){
                    firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
                        var userId = firebase.auth().currentUser.uid;
                        var newuserRef = firebase.database().ref('user_info/' + userId);
                        newuserRef.set({
                            email: email,
                            name: username
                        });
                        nameRef.set({
                            name: username
                        });
                        inputName.value = "";
                        txtEmail.value = "";
                        txtPassword.value = "";
                        create_alert('success','Success Register');
                    })
                    .catch(function(error) {
                        // Handle Errors here.
                        var errorMessage = error.message;
                        inputName.value = "";
                        txtEmail.value = "";
                        txtPassword.value = "";
                        create_alert('error',errorMessage);
                    });
                })
                .catch(function(error) {
                    // Handle Errors here.
                    var errorMessage = error.message;
                    inputName.value = "";
                    txtEmail.value = "";
                    txtPassword.value = "";
                    create_alert('error',errorMessage);
                });
            }
        });
        }
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
        setTimeout(()=>{
            document.location.href = 'index.html';
        },1000);
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};
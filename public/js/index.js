
function init() {
    var user_email = '';

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            // public chat list
            // The html code for post
            var public_before_username = '<div><span class="speaker">';
            var public_before_message = '</span><span class="content">';
            var public_after_content = "</span></div>";
            var mysay_before_username = '<div><span class="speaker mysay">';
            var mysay_before_message = '</span><span class="content mysay">';
            var mysay_after_content = '</span></div><div class="clear"></div>';
            var publicRef = firebase.database().ref('publicChat');
            // List for store posts html
            var public_total_post = [];
            // Counter for checking history post update complete
            var public_first_count = 0;
            // Counter for checking when to update new post
            var public_second_count = 0;
            var publicContent = document.getElementById('public_list');

            publicRef.once('value').then(function (snapshot) {
                    snapshot.forEach(element => {
                        var cur_userID = firebase.auth().currentUser.uid;
                        var publicnameRef = firebase.database().ref('user_info/' + element.val().id);
                        var message = element.val().message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
                        publicnameRef.once('value').then(function(s){
                            if(element.val().id==cur_userID){
                                public_total_post.push(mysay_before_username + s.val().name + mysay_before_message + message + mysay_after_content);
                            }
                            else{
                                public_total_post.push(public_before_username + s.val().name + public_before_message + message + public_after_content);
                            }
                            public_first_count = public_first_count + 1;
                            var newStr = public_total_post.join('');
                            publicContent.innerHTML = newStr;
                        });
                    });
                    
                    publicRef.on('child_added',function(data){
                        public_second_count = public_second_count + 1;
                        if(public_second_count > public_first_count){
                            var cur_userID = firebase.auth().currentUser.uid;
                            var publicnameRef = firebase.database().ref('user_info/' + data.val().id);
                            var message = data.val().message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#039;");
                            publicnameRef.once('value').then(function(s){
                                if(cur_userID == data.val().id){
                                    public_total_post[public_total_post.length] = mysay_before_username + s.val().name + mysay_before_message + message + mysay_after_content;
                                }
                                else{
                                    public_total_post[public_total_post.length] = public_before_username + s.val().name + public_before_message + message + public_after_content;
                                }
                                var newStr = public_total_post.join('');
                                publicContent.innerHTML = newStr;
                            });
                        }
                    });
                })
                .catch(function(e){
                    console.log(e.message);
                });
            
                post_btn = document.getElementById('post_btn');
                post_txt = document.getElementById('comment');

                post_btn.addEventListener('click', function () {
                    if (post_txt.value != "") {
                        var user1 = firebase.auth().currentUser;
                        if(user1){
                            var newpublicref = firebase.database().ref('publicChat').push();
                            newpublicref.set({
                                id: user1.uid,
                                message: post_txt.value
                            });
                        }
                        post_txt.value = "";
                    }
                });



            // user list
            var before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>";
            var before_email = "</h6><div class='media text-muted pt-3'><img src='img/user.svg' alt='' class='mr-2 rounded' style='height:40px; width:40px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>Email</strong>";
            var before_id = "</p><button class='btn btn-warning text-white' id=";
            var after_content = ">Chat</button></div></div>";
            var listRef = firebase.database().ref('user_info');
            // List for store list html
            var total_list = [];
            // Counter for checking history list update complete
            var list_first_count = 0;
            // Counter for checking when to update new list
            var list_second_count = 0;
            var total_btn_id = [];
            listRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(element => {
                    if(element.val().email != user.email){
                        var userName = element.val().name;
                        total_list.push(before_username + userName + before_email + element.val().email + before_id + element.key + after_content);
                        total_btn_id.push(element.key);
                        list_first_count++;
                    }
                });
                var newList = total_list.join('');
                var listContent = document.getElementById('user_list');
                listContent.innerHTML = newList;
                // add function of button
                total_btn_id.forEach(function(e){
                    var btn = document.getElementById(e);
                    btn.addEventListener('click',function(event){
                        var userId = firebase.auth().currentUser.uid;
                        var curchatRef = firebase.database().ref('currentChat/' + userId);
                        var id = event.target.id;
                        curchatRef.set({
                            chatId: id
                        }).then(function(){
                            document.body.style.opacity = 0;
                            setTimeout(function(){
                                document.location.href = "secret_room.html";
                            },1500);
                        });
                    });
                });
                listRef.on('child_added',function(data){
                    var _user = firebase.auth().currentUser;
                    if(data.val().email != _user.email && _user){
                        list_second_count++;
                        if(list_second_count > list_first_count){
                            var userName = data.val().name;
                            total_list[total_list.length]= before_username + userName + before_email + data.val().email + before_id + data.key + after_content;
                            total_btn_id[total_btn_id.length] = data.key;
                            newList = total_list.join('');
                            listContent.innerHTML = newList;
                            var btn = document.getElementById(data.key);
                            btn.addEventListener('click',function(event){
                                var userid = firebase.auth().currentUser.uid;
                                var curchatRef = firebase.database().ref('currentChat/' + userid);
                                var id = event.target.id;
                                curchatRef.set({
                                    chatId: id
                                });
                                document.body.style.opacity = 0;
                                setTimeout(function(){
                                    document.location.href = "secret_room.html";
                                },1500);
                            });                            
                        }
                    }
                });
            })
            .catch(function(e){
                console.log(e.message);
            });

            // google notifictation
            var NotifyRef = firebase.database().ref('notify');
            NotifyRef.once('value').then(function(snapshot){
                var userId = firebase.auth().currentUser.uid;
                snapshot.forEach(e => {
                    if(e.val().receiver == userId){
                        var name2Ref = firebase.database().ref('user_info/' + e.val().sender);
                        name2Ref.once('value').then(function(ss){
                            var sender = ss.val().name;
                            var googleNotify = function(){
                                var notification = new Notification(sender+'傳送了訊息', {
                                    body: e.val().message,
                                    icon: 'img/notify.png'
                                });
                                notification.onclick = function() {
                                    firebase.database().ref('notify/'+ e.key).remove();
                                    notification.close();    
                                };
                            };
                            if (window.Notification) {
                                if (Notification.permission == "granted") {
                                    googleNotify();
                                }
                                else if (Notification.permission != "denied") {
                                    Notification.requestPermission(function (permission) {
                                        googleNotify();
                                    });
                                }
                            } else {
                                create_alert('error','此瀏覽器不支援Notification');    
                            }
                        });
                    }
                });
                NotifyRef.on('child_added',function(shot){
                    if(shot.val().receiver == userId){
                        var name2Ref = firebase.database().ref('user_info/' + shot.val().sender);
                        name2Ref.once('value').then(function(ss){
                            var sender = ss.val().name;
                            var googleNotify = function(){
                                var notification = new Notification(sender+'傳送了訊息', {
                                    body: shot.val().message,
                                    icon: 'img/notify.png'
                                });
                                notification.onclick = function() {
                                    firebase.database().ref('notify/'+ shot.key).remove();
                                    notification.close();    
                                };
                            };
                            if (window.Notification) {
                                if (Notification.permission == "granted") {
                                    googleNotify();
                                }
                                else if (Notification.permission != "denied") {
                                    Notification.requestPermission(function (permission) {
                                        googleNotify();
                                    });
                                }
                            } else {
                                create_alert('error','此瀏覽器不支援Notification');    
                            }
                        });
                    }
                });
            });

            // process logout
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";

            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener('click',function(){
                firebase.auth().signOut().then(function(){
                    create_alert('success','Logout Success');
                    document.getElementById('user_list').innerHTML = "";
                    document.getElementById('public_list').innerHTML = "";
                }).catch(function(error){
                    create_alert('error','Logout Fail');
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('user_list').innerHTML = "";
            document.getElementById('public_list').innerHTML = "";
        }
        // css animation
        var hello = document.getElementById('hello');
        var _name = document.getElementById('userName');
        if(firebase.auth().currentUser){
            var userId = firebase.auth().currentUser.uid;
            var getnameRef = firebase.database().ref('user_info/' + userId);
            getnameRef.once('value').then(function(s){
                var name = s.val().name;
                _name.textContent = 'Hello ' + name;
                hello.addEventListener('mouseover',function(){
                    _name.style.opacity = 1;
                });
                hello.addEventListener('mouseout',function(){
                    _name.style.opacity = 0;
                });
            });
        }
        else {
            _name.textContent = "Please Login";
            hello.addEventListener('mouseover',function(){
                _name.style.opacity = 1;
            });
            hello.addEventListener('mouseout',function(){
                _name.style.opacity = 0;
            });
        }
    });

    // set new user name
    var inputNewName = document.getElementById('inputNewName');
    var set_btn = document.getElementById('set_btn');

    set_btn.addEventListener('click',function(){
        var user = firebase.auth().currentUser;
        var userId = firebase.auth().currentUser.uid;
        var newName = inputNewName.value.toString();
        if(user){
            if(newName == ""){
                create_alert('error','Please enter your new name');
            }
            else{
                var nameRef = firebase.database().ref('exist_name/' + newName);
                nameRef.once('value').then(function (snapshot) {
                    var data = snapshot.val();
                    if(data != null){
                        create_alert('error','This user name is existed');
                        inputNewName.value = "";
                    }
                    else{
                        var oldRef = firebase.database().ref('user_info/' + userId);
                        var oldName;
                        oldRef.once('value').then(function(s){
                            oldName = s.val().name;
                            var deleteRef = firebase.database().ref('exist_name/' + oldName);
                            deleteRef.remove();
                        }).then(function(){
                            oldRef.set({
                                email: user.email,
                                name: newName
                            });
                        });
                        nameRef.set({
                            name: newName
                        });
                        inputNewName.value = "";
                        create_alert('success','Success set name');
                        document.location.href = "index.html";
                    }
                });
            }
        }
        else{
            inputNewName.value = "";
            create_alert('error','You should login first');
        }
        
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    init();
};


window.onload = function(){
    document.body.style.opacity = 1;
    firebase.auth().onAuthStateChanged(function (user) {
    var player = firebase.auth().currentUser.uid;
    var userRef = firebase.database().ref('user_info/' + player);
    userRef.once('value').then(function(s){
        var player_name = s.val().name;
        var playerRef = firebase.database().ref('currentChat/' + player);
        playerRef.once('value').then(function(s){
            var challenger = s.val().chatId;
            var userRef2 = firebase.database().ref('user_info/' + challenger);
            userRef2.once('value').then(function(ss){
                var challenger_name = ss.val().name;
                var gameRef1 = firebase.database().ref('game/' + player + challenger);
                var gameRef2 = firebase.database().ref('game/' + challenger + player);
                gameRef2.once('value').then(function(g2){
                    if(g2.val()!=null){
                        // isGameOver
                        var gameOverRef = firebase.database().ref('game/' + challenger + player +'/' + 'isGameOver');
                        var isGameOver;
                        gameOverRef.once('value').then(function(e){
                            isGameOver = e.val();
                            gameOverRef.on('value',function(changeData){
                                isGameOver = changeData.val();
                                if(isGameOver){
                                    reset_div.style.opacity = 1;
                                    if(!isTied){
                                        result.textContent = cur_turn.textContent + ' Win';
                                    }
                                    else{
                                        result.textContent = 'Tie';
                                    }
                                    $('#who_win').modal('show');
                                }
                            });
                        });
                        // isTied
                        var tiedRef = firebase.database().ref('game/' + challenger + player +'/' + 'isTied');
                        var isTied;
                        tiedRef.once('value').then(function(e){
                            isTied = e.val();
                            tiedRef.on('value',function(changeData){
                                isTied = changeData.val();
                            });
                        });
                        // occupiedNum
                        var occupyNumRef = firebase.database().ref('game/' + challenger + player +'/' + 'occupiedNum');
                        var occupiedNum = 0;
                        occupyNumRef.once('value').then(function(e){
                            occupiedNum = e.val();
                            occupyNumRef.on('value',function(changeData){
                                occupiedNum = changeData.val();
                            });
                        });
                        // player info
                        var playerORef = firebase.database().ref('game/' + challenger + player +'/' + 'player_O');
                        var player_O = document.getElementById('O');
                        var player_X = document.getElementById('X');
                        playerORef.once('value').then(function(e){
                            player_O.textContent = (e.val()==player)? player_name : challenger_name; 
                            player_X.textContent = (e.val()==player)? challenger_name : player_name;
                        });
                        // isOccupied
                        var isOccupiedRef = firebase.database().ref('game/' + challenger + player +'/' + 'isOccupied');
                        var isOccupied = [];
                        isOccupiedRef.once('value').then(function(e){
                            for(let i=0; i<9; i++){
                                isOccupied[i] = e.val()[i];
                            }
                            isOccupiedRef.on('child_changed',function(changeData){
                                isOccupied[changeData.key] = changeData.val();
                            });
                        });
                        // turn
                        var cur_turn = document.getElementById('cur_turn');
                        var turnRef = firebase.database().ref('game/' + challenger + player +'/' + 'turn');
                        var turn = '';
                        turnRef.once('value').then(function(e){
                            turn = e.val();
                            cur_turn.textContent = (turn=='O')? player_O.textContent : player_X.textContent;
                            turnRef.on('value',function(changeData){
                                turn = changeData.val();
                                cur_turn.textContent = (turn=='O')? player_O.textContent : player_X.textContent;
                            });
                        });
                        // cellState
                        var cell = document.querySelectorAll('.cell');
                        var cellStateRef = firebase.database().ref('game/' + challenger + player +'/' + 'cellState');
                        var cellState = [];
                        cellStateRef.once('value').then(function(e){
                            for(let j=0; j<9; j++){
                                cellState[j] = e.val()[j];
                            }
                                var i = 0;
                                cell.forEach(function(c){
                                    c.textContent = cellState[i];
                                    if(cellState[i]!='')
                                        c.style.opacity = 1;
                                    i++;
                                    c.addEventListener('mouseover',function(event){
                                        var id = event.target.id;
                                        if(isOccupied[id] == 0 && !isGameOver && cur_turn.textContent == player_name){
                                            this.textContent = turn;
                                            this.style.opacity = 0.2;
                                        }
                                    });
                                    c.addEventListener('mouseout',function(){
                                        var id = event.target.id;
                                        if(isOccupied[id] == 0 && !isGameOver && cur_turn.textContent == player_name){
                                            this.style.opacity = 0;
                                            this.textContent = '';
                                        }
                                    });
                                    c.addEventListener('click',function(){
                                        if(!isGameOver && cur_turn.textContent == player_name){
                                            var id = event.target.id;
                                            if(isOccupied[id] == 0){
                                                this.style.opacity = 1;
                                                this.textContent = turn;
                                                var isOccupiedNRef = firebase.database().ref('game/' + challenger + player +'/' + 'isOccupied/' + id);
                                                isOccupiedNRef.set(1);
                                                var cellStateNRef = firebase.database().ref('game/' + challenger + player +'/' + 'cellState/' + id);
                                                cellStateNRef.set(turn);
                                                occupyNumRef.set(occupiedNum+1);
                                                if(id==0){
                                                    var condition1 = (cellState[1]==turn && cellState[2]==turn)? true : false;
                                                    var condition2 = (cellState[3]==turn && cellState[6]==turn)? true : false;
                                                    var condition3 = (cellState[4]==turn && cellState[8]==turn)? true : false;
                                                    if(condition1 || condition2 || condition3){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==1){
                                                    var condition1 = (cellState[0]==turn && cellState[2]==turn)? true : false;
                                                    var condition2 = (cellState[4]==turn && cellState[7]==turn)? true : false;
                                                    if(condition1 || condition2){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==2){
                                                    var condition1 = (cellState[0]==turn && cellState[1]==turn)? true : false;
                                                    var condition2 = (cellState[5]==turn && cellState[8]==turn)? true : false;
                                                    var condition3 = (cellState[4]==turn && cellState[6]==turn)? true : false;
                                                    if(condition1 || condition2 || condition3){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==3){
                                                    var condition1 = (cellState[4]==turn && cellState[5]==turn)? true : false;
                                                    var condition2 = (cellState[0]==turn && cellState[6]==turn)? true : false;
                                                    if(condition1 || condition2){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==4){
                                                    var condition1 = (cellState[3]==turn && cellState[5]==turn)? true : false;
                                                    var condition2 = (cellState[1]==turn && cellState[7]==turn)? true : false;
                                                    var condition3 = (cellState[0]==turn && cellState[8]==turn)? true : false;
                                                    var condition4 = (cellState[2]==turn && cellState[6]==turn)? true : false;
                                                    if(condition1 || condition2 || condition3 || condition4){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==5){
                                                    var condition1 = (cellState[3]==turn && cellState[4]==turn)? true : false;
                                                    var condition2 = (cellState[2]==turn && cellState[8]==turn)? true : false;
                                                    if(condition1 || condition2){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==6){
                                                    var condition1 = (cellState[7]==turn && cellState[8]==turn)? true : false;
                                                    var condition2 = (cellState[0]==turn && cellState[3]==turn)? true : false;
                                                    var condition3 = (cellState[2]==turn && cellState[4]==turn)? true : false;
                                                    if(condition1 || condition2 || condition3){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else if(id==7){
                                                    var condition1 = (cellState[6]==turn && cellState[8]==turn)? true : false;
                                                    var condition2 = (cellState[1]==turn && cellState[4]==turn)? true : false;
                                                    if(condition1 || condition2){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                else{
                                                    var condition1 = (cellState[6]==turn && cellState[7]==turn)? true : false;
                                                    var condition2 = (cellState[2]==turn && cellState[5]==turn)? true : false;
                                                    var condition3 = (cellState[0]==turn && cellState[4]==turn)? true : false;
                                                    if(condition1 || condition2 || condition3){
                                                        isGameOver = true;
                                                        gameOverRef.set(true);
                                                    }
                                                }
                                                if(occupiedNum==9 && !isGameOver){
                                                    isTied = true;
                                                    tiedRef.set(true);
                                                    isGameOver = true;
                                                    gameOverRef.set(true);
                                                }
                                                if(isGameOver){
                                                    reset_div.style.opacity = 1;
                                                    if(!isTied){
                                                        if(turn=='O'){
                                                            var times = Number(o_point.textContent);
                                                            times++;
                                                            pointORef.set(times);
                                                        }
                                                        else{
                                                            var times = Number(x_point.textContent);
                                                            times++;
                                                            pointXRef.set(times);
                                                        }
                                                        result.textContent = cur_turn.textContent + ' Win';
                                                    }
                                                    else{
                                                        result.textContent = 'Tie';
                                                    }
                                                }
                                                else{
                                                    if(turn=='O'){
                                                        turnRef.set('X');
                                                    }
                                                    else{
                                                        turnRef.set('O');
                                                    }
                                                }
                                            }
                                        }
                                    });
                                });
                            cellStateRef.on('child_changed',function(changeData){
                                cellState[changeData.key] = changeData.val();
                                var _cell = document.getElementById(changeData.key);
                                _cell.textContent = cellState[changeData.key];
                                if(changeData.val()!='')
                                    _cell.style.opacity = 1;
                            });
                        });
                        // point
                        var pointORef = firebase.database().ref('game/' + challenger + player +'/' + 'o_win_times');
                        var pointXRef = firebase.database().ref('game/' + challenger + player +'/' + 'x_win_times');
                        var o_point = document.getElementById('o_point');
                        var x_point = document.getElementById('x_point');
                        pointORef.once('value').then(function(e){
                            o_point.textContent = e.val();
                            pointORef.on('value',function(changeData){
                                o_point.textContent = changeData.val();
                            });
                        });
                        pointXRef.once('value').then(function(e){
                            x_point.textContent = e.val();
                            pointXRef.on('value',function(changeData){
                                x_point.textContent = changeData.val();
                            });
                        });
                        var result = document.getElementById('result');
                        var reset_div = document.getElementById('reset_div');
                        // reset
                        var resetRef = firebase.database().ref('game/' + challenger + player +'/' + 'isReset');
                        resetRef.once('value').then(function(r){
                            if(r.val()){
                                $('#who_win').modal('hide');
                                result.textContent = '';
                                reset_div.style.opacity = 0;
                            }
                            resetRef.on('value',function(rr){
                                if(rr.val()){
                                    $('#who_win').modal('hide');
                                    result.textContent = '';
                                    reset_div.style.opacity = 0;
                                    resetRef.set(false);
                                }
                            });
                        });
                        var resetBtn = document.getElementById('reset');
                        resetBtn.onclick = function(){
                            $('#who_win').modal('hide');
                            if(player_O.textContent == player_name){
                                turn = (turn=='O')?'X':'O';
                                turnRef.set(turn);
                                gameRef2.set({
                                    player_O: player,
                                    player_X: challenger,
                                    turn: turn,
                                    isOccupied : [0,0,0,0,0,0,0,0,0],
                                    isGameOver: false,
                                    isTied: false,
                                    isReset: true,
                                    occupiedNum: 0,
                                    cellState: ['','','','','','','','',''],
                                    o_win_times: o_point.textContent,
                                    x_win_times: x_point.textContent
                                });
                            }
                            else{
                                turn = (turn=='O')?'X':'O';
                                turnRef.set(turn);
                                gameRef2.set({
                                    player_O: challenger,
                                    player_X: player,
                                    turn: turn,
                                    isOccupied : [0,0,0,0,0,0,0,0,0],
                                    isGameOver: false,
                                    isTied: false,
                                    isReset: true,
                                    occupiedNum: 0,
                                    cellState: ['','','','','','','','',''],
                                    o_win_times: o_point.textContent,
                                    x_win_times: x_point.textContent
                                });
                            }
                            result.textContent = '';
                            reset_div.style.opacity = 0;
                        };        
                    }
                    else{
                        gameRef1.once('value').then(function(g1){
                            if(g1.val()!=null){
                                // isGameOver
                                var gameOverRef = firebase.database().ref('game/' + player + challenger +'/' + 'isGameOver');
                                var isGameOver;
                                gameOverRef.once('value').then(function(e){
                                    isGameOver = e.val();
                                    gameOverRef.on('value',function(changeData){
                                        isGameOver = changeData.val();
                                        if(isGameOver){
                                            reset_div.style.opacity = 1;
                                            if(!isTied){
                                                result.textContent = cur_turn.textContent + ' Win';
                                            }
                                            else{
                                                result.textContent = 'Tie';
                                            }
                                            $('#who_win').modal('show');
                                        }
                                    });
                                });
                                // isTied
                                var tiedRef = firebase.database().ref('game/' + player + challenger +'/' + 'isTied');
                                var isTied;
                                tiedRef.once('value').then(function(e){
                                    isTied = e.val();
                                    tiedRef.on('value',function(changeData){
                                        isTied = changeData.val();
                                    });
                                });
                                // occupiedNum
                                var occupyNumRef = firebase.database().ref('game/' + player + challenger +'/' + 'occupiedNum');
                                var occupiedNum = 0;
                                occupyNumRef.once('value').then(function(e){
                                    occupiedNum = e.val();
                                    occupyNumRef.on('value',function(changeData){
                                        occupiedNum = changeData.val();
                                    });
                                });
                                // player info
                                var playerORef = firebase.database().ref('game/' + player + challenger +'/' + 'player_O');
                                var player_O = document.getElementById('O');
                                var player_X = document.getElementById('X');
                                playerORef.once('value').then(function(e){
                                    player_O.textContent = (e.val()==player)? player_name : challenger_name; 
                                    player_X.textContent = (e.val()==player)? challenger_name : player_name;
                                });
                                // isOccupied
                                var isOccupiedRef = firebase.database().ref('game/' + player + challenger +'/' + 'isOccupied');
                                var isOccupied = [];
                                isOccupiedRef.once('value').then(function(e){
                                    for(let i=0; i<9; i++){
                                        isOccupied[i] = e.val()[i];
                                    }
                                    isOccupiedRef.on('child_changed',function(changeData){
                                        isOccupied[changeData.key] = changeData.val();
                                    });
                                });
                                // turn
                                var cur_turn = document.getElementById('cur_turn');
                                var turnRef = firebase.database().ref('game/' + player + challenger +'/' + 'turn');
                                var turn = '';
                                turnRef.once('value').then(function(e){
                                    turn = e.val();
                                    cur_turn.textContent = (turn=='O')? player_O.textContent : player_X.textContent;
                                    turnRef.on('value',function(changeData){
                                        turn = changeData.val();
                                        cur_turn.textContent = (turn=='O')? player_O.textContent : player_X.textContent;
                                    });
                                });
                                // cellState
                                var cell = document.querySelectorAll('.cell');
                                var cellStateRef = firebase.database().ref('game/' + player + challenger +'/' + 'cellState');
                                var cellState = [];
                                cellStateRef.once('value').then(function(e){
                                    for(let j=0; j<9; j++){
                                        cellState[j]=e.val()[j];
                                    }
                                        var i = 0;
                                        cell.forEach(function(c){
                                            c.textContent = cellState[i];
                                            if(cellState[i]!='')
                                                c.style.opacity = 1;
                                            i++;
                                            c.addEventListener('mouseover',function(event){
                                                var id = event.target.id;
                                                if(isOccupied[id] == 0 && !isGameOver && cur_turn.textContent == player_name){
                                                    this.textContent = turn;
                                                    this.style.opacity = 0.2;
                                                }
                                            });
                                            c.addEventListener('mouseout',function(){
                                                var id = event.target.id;
                                                if(isOccupied[id] == 0 && !isGameOver && cur_turn.textContent == player_name){
                                                    this.style.opacity = 0;
                                                    this.textContent = '';
                                                }
                                            });
                                            c.addEventListener('click',function(){
                                                if(!isGameOver && cur_turn.textContent == player_name){
                                                    var id = event.target.id;
                                                    if(isOccupied[id] == 0){
                                                        this.style.opacity = 1;
                                                        this.textContent = turn;
                                                        var isOccupiedNRef = firebase.database().ref('game/' + player + challenger +'/' + 'isOccupied/' + id);
                                                        isOccupiedNRef.set(1);
                                                        var cellStateNRef = firebase.database().ref('game/' + player + challenger +'/' + 'cellState/' + id);
                                                        cellStateNRef.set(turn);
                                                        occupyNumRef.set(occupiedNum+1);
                                                        if(id==0){
                                                            var condition1 = (cellState[1]==turn && cellState[2]==turn)? true : false;
                                                            var condition2 = (cellState[3]==turn && cellState[6]==turn)? true : false;
                                                            var condition3 = (cellState[4]==turn && cellState[8]==turn)? true : false;
                                                            if(condition1 || condition2 || condition3){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==1){
                                                            var condition1 = (cellState[0]==turn && cellState[2]==turn)? true : false;
                                                            var condition2 = (cellState[4]==turn && cellState[7]==turn)? true : false;
                                                            if(condition1 || condition2){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==2){
                                                            var condition1 = (cellState[0]==turn && cellState[1]==turn)? true : false;
                                                            var condition2 = (cellState[5]==turn && cellState[8]==turn)? true : false;
                                                            var condition3 = (cellState[4]==turn && cellState[6]==turn)? true : false;
                                                            if(condition1 || condition2 || condition3){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==3){
                                                            var condition1 = (cellState[4]==turn && cellState[5]==turn)? true : false;
                                                            var condition2 = (cellState[0]==turn && cellState[6]==turn)? true : false;
                                                            if(condition1 || condition2){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==4){
                                                            var condition1 = (cellState[3]==turn && cellState[5]==turn)? true : false;
                                                            var condition2 = (cellState[1]==turn && cellState[7]==turn)? true : false;
                                                            var condition3 = (cellState[0]==turn && cellState[8]==turn)? true : false;
                                                            var condition4 = (cellState[2]==turn && cellState[6]==turn)? true : false;
                                                            if(condition1 || condition2 || condition3 || condition4){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==5){
                                                            var condition1 = (cellState[3]==turn && cellState[4]==turn)? true : false;
                                                            var condition2 = (cellState[2]==turn && cellState[8]==turn)? true : false;
                                                            if(condition1 || condition2){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==6){
                                                            var condition1 = (cellState[7]==turn && cellState[8]==turn)? true : false;
                                                            var condition2 = (cellState[0]==turn && cellState[3]==turn)? true : false;
                                                            var condition3 = (cellState[2]==turn && cellState[4]==turn)? true : false;
                                                            if(condition1 || condition2 || condition3){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else if(id==7){
                                                            var condition1 = (cellState[6]==turn && cellState[8]==turn)? true : false;
                                                            var condition2 = (cellState[1]==turn && cellState[4]==turn)? true : false;
                                                            if(condition1 || condition2){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        else{
                                                            var condition1 = (cellState[6]==turn && cellState[7]==turn)? true : false;
                                                            var condition2 = (cellState[2]==turn && cellState[5]==turn)? true : false;
                                                            var condition3 = (cellState[0]==turn && cellState[4]==turn)? true : false;
                                                            if(condition1 || condition2 || condition3){
                                                                isGameOver = true;
                                                                gameOverRef.set(true);
                                                            }
                                                        }
                                                        if(occupiedNum==9 && !isGameOver){
                                                            isTied = true;
                                                            tiedRef.set(true);
                                                            isGameOver = true;
                                                            gameOverRef.set(true);
                                                        }
                                                        if(isGameOver){
                                                            reset_div.style.opacity = 1;
                                                            if(!isTied){
                                                                if(turn=='O'){
                                                                    var times = Number(o_point.textContent);
                                                                    times++;
                                                                    pointORef.set(times);
                                                                }
                                                                else{
                                                                    var times = Number(x_point.textContent);
                                                                    times++;
                                                                    pointXRef.set(times);
                                                                }
                                                                result.textContent = cur_turn.textContent + ' Win';
                                                            }
                                                            else{
                                                                result.textContent = 'Tie';
                                                            }
                                                        }
                                                        else{
                                                            if(turn=='O'){
                                                                turnRef.set('X');
                                                            }
                                                            else{
                                                                turnRef.set('O');
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        });
                                    cellStateRef.on('child_changed',function(changeData){
                                        cellState[changeData.key] = changeData.val();
                                        document.getElementById(changeData.key).textContent = cellState[changeData.key];
                                        if(changeData.val()!='')
                                            document.getElementById(changeData.key).style.opacity = 1;
                                    });
                                });
                                // point
                                var pointORef = firebase.database().ref('game/' + player + challenger +'/' + 'o_win_times');
                                var pointXRef = firebase.database().ref('game/' + player + challenger +'/' + 'x_win_times');
                                var o_point = document.getElementById('o_point');
                                var x_point = document.getElementById('x_point');
                                pointORef.once('value').then(function(e){
                                    o_point.textContent = e.val();
                                    pointORef.on('value',function(changeData){
                                        o_point.textContent = changeData.val();
                                    });
                                });
                                pointXRef.once('value').then(function(e){
                                    x_point.textContent = e.val();
                                    pointXRef.on('value',function(changeData){
                                        x_point.textContent = changeData.val();
                                    });
                                });
                                var result = document.getElementById('result');
                                var reset_div = document.getElementById('reset_div');
                                // reset
                                var resetRef = firebase.database().ref('game/' + player + challenger +'/' + 'isReset');
                                resetRef.once('value').then(function(r){
                                    if(r.val()){
                                        $('#who_win').modal('hide');
                                        result.textContent = '';
                                        reset_div.style.opacity = 0;
                                    }
                                    resetRef.on('value',function(rr){
                                        if(rr.val()){
                                            $('#who_win').modal('hide');
                                            result.textContent = '';
                                            reset_div.style.opacity = 0;
                                            resetRef.set(false);
                                        }
                                    });
                                });
                                var resetBtn = document.getElementById('reset');
                                resetBtn.onclick = function(){
                                    $('#who_win').modal('hide');
                                    if(player_O.textContent == player_name){
                                        turn = (turn=='O')?'X':'O';
                                        turnRef.set(turn);
                                        gameRef1.set({
                                            player_O: player,
                                            player_X: challenger,
                                            turn: turn,
                                            isOccupied : [0,0,0,0,0,0,0,0,0],
                                            isGameOver: false,
                                            isTied: false,
                                            isReset: true,
                                            occupiedNum: 0,
                                            cellState: ['','','','','','','','',''],
                                            o_win_times: o_point.textContent,
                                            x_win_times: x_point.textContent
                                        });
                                    }
                                    else{
                                        turn = (turn=='O')?'X':'O';
                                        turnRef.set(turn);
                                        gameRef1.set({
                                            player_O: challenger,
                                            player_X: player,
                                            turn: turn,
                                            isOccupied : [0,0,0,0,0,0,0,0,0],
                                            isGameOver: false,
                                            isTied: false,
                                            isReset: true,
                                            occupiedNum: 0,
                                            cellState: ['','','','','','','','',''],
                                            o_win_times: o_point.textContent,
                                            x_win_times: x_point.textContent
                                        });
                                    }
                                    result.textContent = '';
                                    reset_div.style.opacity = 0;
                                };        
                            }
                            else{
                                gameRef1.set({
                                    player_O: player,
                                    player_X: challenger,
                                    turn: 'O',
                                    isOccupied : [0,0,0,0,0,0,0,0,0],
                                    isGameOver: false,
                                    isTied: false,
                                    isReset: false,
                                    occupiedNum: 0,
                                    cellState: ['','','','','','','','',''],
                                    o_win_times: 0,
                                    x_win_times: 0
                                });
                                location.href="game.html";
                            }
                        });
                    }
                });
            });
        });
    });
                // google notifictation
                var NotifyRef = firebase.database().ref('notify');
                NotifyRef.once('value').then(function(snapshot){
                    var userId = firebase.auth().currentUser.uid;
                    snapshot.forEach(e => {
                        if(e.val().receiver == userId){
                            var name2Ref = firebase.database().ref('user_info/' + e.val().sender);
                            name2Ref.once('value').then(function(ss){
                                var sender = ss.val().name;
                                var googleNotify = function(){
                                    var notification = new Notification(sender+'傳送了訊息', {
                                        body: e.val().message,
                                        icon: 'img/notify.png'
                                    });
                                    notification.onclick = function() {
                                        firebase.database().ref('notify/'+ e.key).remove();
                                        notification.close();    
                                    };
                                };
                                if (window.Notification) {
                                    if (Notification.permission == "granted") {
                                        googleNotify();
                                    }
                                    else if (Notification.permission != "denied") {
                                        Notification.requestPermission(function (permission) {
                                            googleNotify();
                                        });
                                    }
                                } else {
                                    create_alert('error','此瀏覽器不支援Notification');    
                                }
                            });
                        }
                    });
                    NotifyRef.on('child_added',function(shot){
                        if(shot.val().receiver == userId){
                            var name2Ref = firebase.database().ref('user_info/' + shot.val().sender);
                            name2Ref.once('value').then(function(ss){
                                var sender = ss.val().name;
                                var googleNotify = function(){
                                    var notification = new Notification(sender+'傳送了訊息', {
                                        body: shot.val().message,
                                        icon: 'img/notify.png'
                                    });
                                    notification.onclick = function() {
                                        firebase.database().ref('notify/'+ shot.key).remove();
                                        notification.close();    
                                    };
                                };
                                if (window.Notification) {
                                    if (Notification.permission == "granted") {
                                        googleNotify();
                                    }
                                    else if (Notification.permission != "denied") {
                                        Notification.requestPermission(function (permission) {
                                            googleNotify();
                                        });
                                    }
                                } else {
                                    create_alert('error','此瀏覽器不支援Notification');    
                                }
                            });
                        }
                    });
                });
});
};
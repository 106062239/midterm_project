# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [EnjoyChat]
* Key functions (add/delete)
    1. [所有人一起聊天]
    2. [一對一聊天]
    3. [顯示聊天歷史紀錄]
    
* Other functions (add/delete)
    1. [可以改暱稱]
    2. [一對一聊天時，可以幫對方講的話按讚或按不喜歡]
    3. [一對一聊天區的navbar有一個game的button，按下去可以和對方玩圈圈叉叉]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-hw.firebaseapp.com]

# Components Description : 
1. [Sign In] : [網路上抓的模板]
2. [Sign Up] : [滑鼠移到畫面中的聊天圖示，會有縮放和翻轉的css特效]
3. [third-part accounts] : [google，如果是用google登入的話，一開始沒有暱稱，要先去設一次暱稱]
4. [主畫面] : 
* navbar右上角可以改暱稱。
* 畫面中上方的房子圖示，滑鼠移上去可以旋轉。
* 打招呼的人的圖示，滑鼠移上去會放大且閃爍，並在左邊淡入Hello [username] or Please Login，滑鼠移開時，左邊淡出。
* 自己講的訊息會靠右，別人講的會靠左。
* 最下方是其他會員的基本資料清單，點擊他的chat button可以和他一對一聊天，點完chat button整個畫面會淡出(類似轉場的概念)。
5. [Secret Room] :
* 一開始會淡入畫面。
* 自己講的訊息會靠右，別人講的會靠左。
* 可以幫對方講的訊息按喜歡或不喜歡。
* 滑鼠移到訊息旁的喜歡或不喜歡，會翻轉一次，向上手指變成向下，向下手指變成向上，滑鼠移開會復原。
* navbar的back button可以返回主畫面，game button可以和對方玩圈圈叉叉，有和之前一樣的轉場動畫。
6. [google notification] : [當別人在secret room向你發送訊息時，如果你在主畫面、遊戲室、不是跟發送者同一聊天室，會收到google通知]

# Other Functions Description(1~10%) : 
1. [改暱稱] : [主畫面的右上角可以輸入新暱稱，如果此暱稱已經存在了，會顯示錯誤訊息，如果是google登入，要先來這裡設暱稱]
2. [按喜歡或不喜歡] : [在一對一聊天中，點擊對方訊息的區塊會顯示出喜歡或不喜歡的手指圖示，再次點擊區塊會關閉。滑鼠移到手指上面會縮放，點擊的話訊息欄位會出現喜歡或不喜歡，如果已經有點了喜歡，再點一次會將喜歡取消，如果點不喜歡，會將喜歡蓋掉，只留下不喜歡，反之亦然]
3. [圈圈叉叉] :
* 想返回聊天畫面可以按上一步。
* 會分好誰是圈，誰是叉(第一次先點game的人會是圈)，這是固定的。
* 最上方是比分的歷史紀錄，下面一點是顯示當前換誰下。
* 當前下的人滑鼠移到九宮格內，如果那格還沒被占據，會淡入透明的圈或叉(看你是哪一個)，移出就會淡出，點擊的話就會顯示不透明的圈或叉。
* 遊戲結束會出現一個modal通知遊戲結果，平手(tie)或誰贏，且顯示出reset button，點擊可以玩新的一局。下一局由輸的人先下，如果平手，也是換人先下。
* 玩到一半想退出可以直接退出，遊戲紀錄會保持著。

## Security Report (Optional)
* 讀取使用者清單資料時，會判斷是否有登入才會顯示，所以只有會員能看到其他會員的基本資料。
* 如果是在public room的環境，讀取聊天資料時，會判斷是否有登入才會顯示，所以要是會員才能看到公眾的聊天訊息。
* 如果是在secret room的環境，讀取聊天資料時，會判斷當前兩個使用者的uid是否和資料庫內的一致，是的話才顯示聊天訊息，這樣的話一對一的聊天內容就只有彼此才知道。
* 將會影響到html的符號用html特殊字元的編碼替換，這樣聊天時輸入html內容也會變成文字顯示，不會嵌入html。
